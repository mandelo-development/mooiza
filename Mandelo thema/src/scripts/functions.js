
export const setActive = (el, active) => {
    const formField = el.parentNode;
    if (active) {
        formField.classList.add('form-field--is-active');
    } else {
        formField.classList.remove('form-field--is-active');
        el.value === '' ?
            formField.classList.remove('form-field--is-filled') :
            formField.classList.add('form-field--is-filled');
    }
};

