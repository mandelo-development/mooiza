
import {setActive} from './functions';
import '../../../config/node_modules/bootstrap/js/dist/collapse';
var $ = require("../../../config/node_modules/jquery");

async function general() {
    $(document).ready( function () {
        /* FORM LABELS */ 
        [].forEach.call(
            document.querySelectorAll('.form-field__label, .form-field input, .form-field__textarea'),
            el => {
                el.onblur = () => {
                    setActive(el, false);
        
                };
                el.onfocus = () => {
                    setActive(el, true);
                };
        });

        if(parent.document.getElementById('config-bar')) {
            $('html').addClass('config-mode');
        } else {
            
        }
    });
    
} export {
    general
}