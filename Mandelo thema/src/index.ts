import '../../config/node_modules/regenerator-runtime/runtime';
import './scripts/functions';
import './scripts/updatecss';
import {navigation} from './scripts/navigation';
import './scripts/swiper';
import './scripts/form';
import {filter} from './scripts/filter'; 
import {general} from './scripts/general';
import {lazyload} from './scripts/lazyload';
import './styles/style';

navigation();
filter();
general();
lazyload(document);
